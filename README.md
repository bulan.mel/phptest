# Q-Agency PHP Examination



## Getting started

## Name
Mel Bulan

## Description
Simple Data Processing Examination using API - Laravel



## Installation

Clone repository

```
git clone https://gitlab.com/bulan.mel/phptest.git
```

Run Composer
```
composer install
```

Make new configuration file
```
cp .env.example .env
```

Update Database Credentials
```
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=<Your Database>
DB_USERNAME=<Your Username>
DB_PASSWORD=<Your Password>
```

Add Config to .env
```
#API
API_URL='https://symfony-skeleton.q-tests.com/'
```

Generate App Key
```
php artisan key:generate
```

Run migration files
```
php artisan migrate
```

Run Database seeder file
```
php artisan db:seed --class=UserSeeder
```

Run project
```
php artisan serve
```

Open new Terminal tab, same directory
```
npm install

npm run watch
```

Login Credentials
```
Email: ahsoka.tano@q.agency
Password: Kryze4President
```

## Comments and Suggestions
This symfony api is awesome! Great Work Guys! I'd love to work with you in the future. Once I passed the examination :)

I just think that it would be better when retrieving the list of authors, we can add aditional attributes `number_of_books: <total books of author>`
that way we don't need to loop each author to check if he have books.

Note: This is just for me, no offense meant and if there are any other way of doing this I'd love to learn from you guys.

What I did to Authors List is first fetch all authors, then create DataTable instance with the result from fetch as source.
In DataTable custom function addColumn, that is where I perform the checking if the author have books then disabled delete button if he have books.

The Loading is quite slow due to sending request for each author.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
