<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Http;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Session;
use Hash;
use App\Models\User;
use Carbon\Carbon;

class AuthController extends Controller
{
    //
    public function login(Request $request){
        $data['email'] = $request->email;
        $data['password'] = $request->password;
        $url = env('API_URL').'api/v2/token';
        $currentDateTime = Carbon::now();
        $newDateTime = Carbon::now()->addMinutes(30);
        try {
            $client = new Client();

            $res = $client->request('POST', $url, [
                'body' => json_encode($data),
                'headers' => [
                        'Content-Type' => 'application/json',
                    ]
            ]);
            $response = json_decode($res->getBody(), true);

            if($res->getStatusCode() == 200) {
                $userResponse = $response['user'];
                $userData['name'] = $userResponse['first_name']. ' '. $userResponse['last_name'];
                $userData['password'] = Hash::make($request->password);
                $userData['token'] = $response['token_key'];
                $userData['token_expiry'] = $newDateTime->format('Y-m-d H:i:s');
                $user = User::updateOrCreate([
                    'email' => $request->email
                ],$userData);
            }
            Auth::login($user, $remember = true);
        } catch (\Exception $exception) {
            return 'Caught exception: '. $exception->getMessage();
        }
        
        return redirect('home');
    }
    /**
     * Log out account user.
     *
     * @return \Illuminate\Routing\Redirector
     */
    public function logout(){
        
        Session::flush();
        
        Auth::logout();

        return redirect('login');
    
    }
}
