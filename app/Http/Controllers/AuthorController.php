<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Http;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use Illuminate\Database\Eloquent\Collection;
use Yajra\DataTables\DataTables;
use Session;
use App\Traits\ApiRequest;
use App\Http\Resources\AuthorResource;
use carbon\Carbon;

class AuthorController extends Controller
{
    //
    use ApiRequest;

    public function index(){
        $expired = $this->checkTokenExpiry();
        if($expired) {
            Auth::logout();
            return redirect('login');
        }
        $authors = $this->getAuthors();
        return view('author');
    }

    public function fetchAuthors(){
        $authors = $this->getAuthors();
        $data = new Collection($authors['items']);
        
        return Datatables::of($data)
            ->addColumn('action', function($row){
                    $book = $this->getAuthorsById($row['id']);
                    $data = new Collection($book['books']);
                    $btn = '<a href="'.route('authors.view', $row['id']).'" class="view btn btn-info btn-sm">View</a>';
                    $btn = $btn.'<a href="javascript:void(0)" class="edit btn btn-primary btn-sm">Edit</a>';
                    $btn = $btn.'<a href="'.route('authors.delete', $row['id']).'" class="delete btn btn-danger btn-sm '.(count($data) >= 1 ? 'disabled': '').'" >Delete</a>';
    
                    return $btn;
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    public function show($id) {
        $authors = $this->getAuthorsById($id);
        //dd($authors);
        $authorData['id'] = $authors['id'];
        $authorData['first_name'] = $authors['first_name'];
        $authorData['last_name'] = $authors['last_name'];
        $authorData['biography'] = $authors['biography'];
        $authorData['gender'] = $authors['gender'];
        $authorData['birthday'] = $authors['birthday'];
        $authorData['place_of_birth'] = $authors['place_of_birth'];
        //dd($authorData);
        return view('author-view',compact('authorData'));
    }

    public function showAuthorsBooks($id){
        $authors = $this->getAuthorsById($id);
        $data = new Collection($authors['books']);
        
        return  Datatables::of($data)
            ->addColumn('action', function($row){
                    $btn = '<a href="javascript:void(0)" class="edit btn btn-primary btn-sm">Edit</a>';
                    $btn = $btn.'<a href="'.route('books.delete', $row['id']).'" class="delete btn btn-danger btn-sm" >Delete</a>';

                    return $btn;
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    public function add() {
        return view('author-add');
    }

    public function store(Request $request) {
        $data['first_name'] = $request->first_name;
        $data['last_name'] = $request->last_name;
        $data['biography'] = $request->biography;
        $data['birthday'] = $request->birthday;
        $data['gender'] = $request->gender;
        $data['place_of_birth'] = $request->place_of_birth;

        $saveAuthor = $this->createAuthor($data);

        $res = json_decode($saveAuthor->content(), true);

        Session::flash('author-message', $res['message']); 
        if($res['status'] == "Success") {
            Session::flash('alert-class', 'alert-info'); 
        } else {
            Session::flash('alert-class', 'alert-danger');
        }
        return redirect('/author/add');
    }

    public function destroy($id) {
        $deleteAuthor = $this->deleteAuthor($id);
        $res = json_decode($deleteAuthor->content(), true);

        Session::flash('message', $res['message']); 
        if($res['status'] == "Success") {
            Session::flash('alert-class', 'alert-info'); 
        } else {
            Session::flash('alert-class', 'alert-danger');
        }
        return back();
    }

        
}
