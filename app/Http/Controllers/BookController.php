<?php

namespace App\Http\Controllers;
use Session;
use Illuminate\Http\Request;
use App\Traits\ApiRequest;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class BookController extends Controller
{
    //
    use ApiRequest;

    public function index(){
        $expired = $this->checkTokenExpiry();
        if($expired) {
            Auth::logout();
            return redirect('login');
        }
        $authors = $this->getAuthors();
        foreach ($authors['items'] as $key => $value) {
            $optAuthors[] = ['id' => $value['id'],'name' => $value['first_name'] .' '. $value['last_name']];
        }
        return view('book',compact('optAuthors'));
    }

    public function store(Request $request) {

        $data['author']['id'] = $request->author_id;
        $data['title'] = $request->title;
        $data['release_date'] = $request->release_date;
        $data['description'] = $request->description;
        $data['isbn'] = $request->isbn;
        $data['format'] = $request->format;
        $data['number_of_pages'] = (int)$request->number_of_pages;

        $saveBook = $this->createBook($data);

        $res = json_decode($saveBook->content(), true);

        Session::flash('message', $res['message']); 
        if($res['status'] == "Success") {
            Session::flash('alert-class', 'alert-info'); 
        } else {
            Session::flash('alert-class', 'alert-danger');
        }
        return redirect('/book');
    }

    public function destroy($id) {
        $deleteBook = $this->deleteBook($id);
        $res = json_decode($deleteBook->content(), true);

        Session::flash('message', $res['message']); 
        if($res['status'] == "Success") {
            Session::flash('alert-class', 'alert-info'); 
        } else {
            Session::flash('alert-class', 'alert-danger');
        }
        return back();
    }
}
