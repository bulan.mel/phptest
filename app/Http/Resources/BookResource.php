<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class BookResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return return [
            'id' => $this->id,
            'title' => $this->title,
            'release_date' => $this->release_date,
            'isbn' => $this->isbn,
            'format' => $this->format,
            'number_of_pages' => $this->number_of_pages
        ];
    }
}
