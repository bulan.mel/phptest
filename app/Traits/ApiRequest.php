<?php

namespace App\Traits;

use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Auth;

/*
 |- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
 | Api Request Traits
 |- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
 | This trait will be use for any request we sent to client
 |
 |
 */

 trait ApiRequest
 {
    /**
     * Get All Authors
     */
    protected function getAuthors() {
        $url = env('API_URL').'api/v2/authors';
        $token = Auth::user()->token;
        $client = new Client();
        
        $response = $client->request('GET', $url, [
            'query' => [ 
                'query' =>'',
                'orderBy' => 'id',
                'direction' => 'ASC',
                'limit' => 100,
                'page' => 1
            ],
            'headers' => [
                    'Content-Type' => 'application/json',
                    'Authorization' => 'Bearer ' . $token,
                ]
        ]);

        return json_decode($response->getBody(), true); 
    }

     /**
     * Get Authors By Id
     */
    protected function getAuthorsById($id) {
        $url = env('API_URL').'api/v2/authors/'.$id;
        $token = Auth::user()->token;
        $client = new Client();
        
        $response = $client->request('GET', $url, [
            'query' => [ 
                'query' =>'',
                'orderBy' => 'id',
                'direction' => 'ASC',
                'limit' => 100,
                'page' => 1
            ],
            'headers' => [
                    'Content-Type' => 'application/json',
                    'Authorization' => 'Bearer ' . $token,
                ]
        ]);

        return json_decode($response->getBody(), true); 
    }

    /**
     * Create Author
     */

    protected function createAuthor($data) {
        try {
            $url = env('API_URL').'api/v2/authors';
            $token = Auth::user()->token;
            $client = new Client();

            $response = $client->request('POST', $url, [
                'body' => json_encode($data),
                'headers' => [
                        'Content-Type' => 'application/json',
                        'Authorization' => 'Bearer ' . $token,
                    ]
            ]);

            if($response->getStatusCode() == 200){
                return $this->success(json_decode($response->getBody(), true),'Successfully Created Author!');
            }

        } catch (\Exception $exception) {
            return $this->error($exception->getMessage(),$response->getStatusCode());
        }
    }

    /**
     * Delete Author
     */

    protected function deleteAuthor($id) {
        try {
            $url = env('API_URL').'api/v2/authors/'.$id;
            $token = Auth::user()->token;
            $client = new Client();

            $response = $client->request('DELETE', $url, [
                'headers' => [
                        'Content-Type' => 'application/json',
                        'Authorization' => 'Bearer ' . $token,
                    ]
            ]);
            if($response->getStatusCode() == 204){
                return $this->success(null, 'Successfully Deleted Author!');
            }

        } catch (\Exception $exception) {
            return $this->error($exception->getMessage(),$response->getStatusCode());
        }
    }

    /**
     * Create Book
     */

    protected function createBook($data) {
        try {
            $url = env('API_URL').'api/v2/books';
            $token = Auth::user()->token;
            $client = new Client();

            $response = $client->request('POST', $url, [
                'body' => json_encode($data),
                'headers' => [
                        'Content-Type' => 'application/json',
                        'Authorization' => 'Bearer ' . $token,
                    ]
            ]);

            if($response->getStatusCode() == 200){
                return $this->success(json_decode($response->getBody(), true),'Successfully Created Book!');
            }

        } catch (\Exception $exception) {
            return $this->error($exception->getMessage(),$response->getStatusCode());
        }
    }

    /**
     * Delete Book
     */

    protected function deleteBook($id) {
        try {
            $url = env('API_URL').'api/v2/books/'.$id;
            $token = Auth::user()->token;
            $client = new Client();

            $response = $client->request('DELETE', $url, [
                'headers' => [
                        'Content-Type' => 'application/json',
                        'Authorization' => 'Bearer ' . $token,
                    ]
            ]);
            if($response->getStatusCode() == 204){
                return $this->success(null, 'Successfully Deleted Book!');
            }

        } catch (\Exception $exception) {
            return $this->error($exception->getMessage(),$response->getStatusCode());
        }
    }



    /**
     * Return a success JSON response.
     *
     * @param  array|string  $data
     * @param  string  $message
     * @param  int|null  $code
     * @return \Illuminate\Http\JsonResponse
     */
	protected function success($data, string $message = null, int $code = 200)
	{
		return response()->json([
			'status' => 'Success',
			'message' => $message,
			'data' => $data
		], $code);
	}

    /**
     * Return an error JSON response.
     *
     * @param  string  $message
     * @param  int  $code
     * @param  array|string|null  $data
     * @return \Illuminate\Http\JsonResponse
     */
	protected function error(string $message = null, int $code, $data = null)
	{
		return response()->json([
			'status' => 'Error',
			'message' => $message,
			'data' => $data
		], $code);
	}

    /**
     * Check Token Expiry
     * 
     */

     protected function checkTokenExpiry(){
        if (Carbon::now()->gt(Auth::user()->token_expiry)) {
            return true;
        }
     }

 }