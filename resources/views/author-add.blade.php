@extends('layouts.app')

@section('content')

    <div class="content-header">
        <div class="container-fluid">   
            <div class="row-mb-2">   
                <div class="col-sm-6"> 
                    <h1 class="m-0">Add Author</h1>
                </div>
            </div>
        </div>
    </div>
    <section class="content">
        @if(Session::has('author-message'))
            <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('author-message') }}</p>
        @endif
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-6">
                 <form method="POST" action="{{route('authors.store')}}">
                <!-- CROSS Site Request Forgery Protection -->
                @csrf
                @method('POST')
                <div class="form-group">
                    <label>First Name</label>
                    <input type="text" class="form-control" name="first_name" id="first_name">
                </div>
                <div class="form-group">
                    <label>Last Name</label>
                    <input type="text" class="form-control" name="last_name" id="last_name">
                </div>
                <div class="form-group">
                    <label>Birthdate</label>
                    <input type="text" placeholder="YYYY-MM-DD" class="form-control" name="birthday" id="birthday">
                </div>
                <div class="form-group">
                    <label>Biography</label>
                    <input type="text" class="form-control" name="biography" id="biography">
                </div>
                <div class="form-group">
                    <label>Gender</label><br>
                    <input type="radio" id="female" name="gender" value="female">
                    <label for="female">Female</label><br>
                    <input type="radio" id="male" name="gender" value="male">
                    <label for="male">Male</label><br>
                </div>
                <div class="form-group">
                    <label>Place of Birth</label>
                    <input type="text" class="form-control" name="place_of_birth" id="place_of_birth">
                </div>
                <input type="submit" name="send" value="Submit" class="btn btn-dark btn-block">
            </form>
                </div>
            </div>
        </div>
    </section>

@endsection