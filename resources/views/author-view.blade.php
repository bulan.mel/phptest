@extends('layouts.app')

@section('content')

    <div class="content-header">
        <div class="container-fluid">   
            <div class="row-mb-2">   
                <div class="col-sm-6"> 
                    <h1 class="m-0">Author Details</h1>
                </div>
            </div>
        </div>
    </div>
    <section class="content">
        @if(Session::has('message'))
            <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
        @endif
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                        <!-- CROSS Site Request Forgery Protection -->
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>First Name</label>
                                    <input type="text" readonly class="form-control" name="first_name" id="first_name" value="{{$authorData['first_name']}}">
                                </div>
                                <div class="form-group">
                                    <label>Last Name</label>
                                    <input type="text" readonly class="form-control" name="last_name" id="last_name" value="{{$authorData['last_name']}}">
                                </div>
                                <div class="form-group">
                                    <label>Birthdate</label>
                                    <input type="text" readonly placeholder="YYYY-MM-DD" class="form-control" name="birthday" id="birthday" value="{{$authorData['birthday']}}">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Biography</label>
                                    <input type="text" readonly class="form-control" name="biography" id="biography" value="{{$authorData['biography']}}">
                                </div>
                                <div class="form-group">
                                    <label>Gender</label><br>
                                    <input type="text" readonly class="form-control" name="gender" id="gender" value="{{$authorData['gender']}}">
                                </div>
                                <div class="form-group">
                                    <label>Place of Birth</label>
                                    <input type="text" readonly class="form-control" name="place_of_birth" id="place_of_birth" value="{{$authorData['place_of_birth']}}">
                                </div>
                            </div>
                        </div>
                </div>
            </div>
            <div class="row-mb-2">   
                <div class="col-sm-6"> 
                    <h1 class="m-0">Books</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="card">
                    <!-- /.card-header -->
                        <div class="card-body">
                            <table id="author-books" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>Title</th>
                                <th>Release Date</th>
                                <th>ISBN</th>
                                <th>Format</th>
                                <th>No. of Pages</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                            </table>
                        </div>
                    <!-- /.card-body -->
                    </div>
                <!-- /.card -->
                </div>
            <!-- /.col -->
            </div>
        </div>
    </section>

@endsection
@section('scripts')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>  
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
<script>
$('#author-books').dataTable({
        processing: true,
        serverSide: true,
        ajax: "{{ route('authors.books', $authorData['id']) }}",
        columns: [
            {data: 'title', name: 'title'},
            {data: 'release_date', name: 'release_date'},
            {data: 'isbn', name: 'isbn', orderable: false, searchable: false},
            {data: 'format', name: 'gender'},
            {data: 'number_of_pages', name: 'number_of_pages'},
            {data: 'action', name: 'action', orderable: false, searchable: false},
        ],
        order: [[0, 'desc']],
        displayLength: -1
    }).fnFilterOnReturn();

</script>
@endsection