@extends('layouts.app')

@section('content')

    <div class="content-header">
        <div class="container-fluid">   
            <div class="row-mb-2">   
                <div class="col-sm-6"> 
                    <h1 class="m-0">Add Books</h1>
                </div>
            </div>
        </div>
    </div>
    <section class="content">
        @if(Session::has('message'))
        <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
        @endif
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-6">
                 <form method="POST" action="{{route('books.store')}}">
                <!-- CROSS Site Request Forgery Protection -->
                @csrf
                @method('POST')
                <div class="form-group">
                    <label>Author</label>

                <select class="form-control" name="author_id">
                    <option>Select Author</option>
                    @foreach ($optAuthors as $authors)
                        <option value="{{$authors['id']}}"> {{$authors['name']}}</option>
                    @endforeach    
                </select>
                </div>
                <div class="form-group">
                    <label>Title</label>
                    <input type="text" class="form-control" name="title" id="title">
                </div>
                <div class="form-group">
                    <label>Desription</label>
                    <input type="text" class="form-control" name="description" id="description">
                </div>
                <div class="form-group">
                    <label>Release Date</label>
                    <input type="text" placeholder="YYYY-MM-DD" class="form-control" name="release_date" id="release_date">
                </div>
                <div class="form-group">
                    <label>ISBN</label><br>
                    <input type="text" class="form-control" name="isbn" id="isbn">
                </div>
                <div class="form-group">
                    <label>Format</label>
                    <input type="text" class="form-control" name="format" id="format">
                </div>
                <div class="form-group">
                    <label>No. of Pages</label>
                    <input type="text" class="form-control" name="number_of_pages" id="number_of_pages">
                </div>
                <input type="submit" name="send" value="Submit" class="btn btn-dark btn-block">
            </form>
                </div>
            </div>
        </div>
    </section>

@endsection
