<script src={{asset("js/plugins/jquery/jquery.min.js")}}></script>

<script src={{asset("js/plugins/bootstrap/js/bootstrap.bundle.min.js")}}></script>

<script src={{asset("js/dist/js/adminlte.js?v=3.2.0")}}></script>

<script src={{asset("js/plugins/chart.js/Chart.min.js")}}></script>
{{-- <script src={{asset("js/dist/js/demo.js")}}></script> --}}

<script src={{asset("js/dist/js/pages/dashboard3.js")}}></script>


@yield('scripts')