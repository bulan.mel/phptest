<aside class="main-sidebar sidebar-dark-primary elevation-4">

<a href="{{route('home')}}" class="brand-link">
    <img src={{asset("img/AdminLTELogo.png")}} alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
    <span class="brand-text font-weight-light">Q-Agency</span>
</a>

<div class="sidebar">
    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
            <img src={{asset("img/user2-160x160.jpg")}} class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
            <a href="#" class="d-block">{{Auth::user()->name}}</a>
        </div>
    </div>

    <div class="form-inline">
        <div class="input-group" data-widget="sidebar-search">
            <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
        <div class="input-group-append">
            <button class="btn btn-sidebar">
                <i class="fas fa-search fa-fw"></i>
            </button>
        </div>
    </div>
</div>

<nav class="mt-2">
    <ul class="nav nav-pills nav-sidebar flex-column"  role="menu" data-accordion="false">

    <li class="nav-item" >
    <a href="{{ route('authors') }}" class="nav-link {{ (request()->is('author')) ? 'active' : '' }}">
        <i class="nav-icon fas fa-user"></i>
        <p>
            Author
        </p>
    </a>

    <li class="nav-item">
        <a href="{{ route('books') }}" class="nav-link {{ (request()->is('book')) ? 'active' : '' }}">
        <i class="fas fa-book nav-icon"></i>
            <p>
                Books
            </p>
        </a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="{{ route('logout') }}"
            onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();">
            <i class="fas fa-sign-out-alt nav-icon"></i>
            <p>
                Logout
            </p>
        </a>
        
    </li>
    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
            @csrf
        </form>
    
    </ul>
</nav>
</aside>