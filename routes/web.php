<?php


use App\Http\Controllers\AuthController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\AuthorController;
use App\Http\Controllers\BookController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('login');
});
// Auth::routes([]);
Route::get('login',[LoginController::class,'index']);

Route::post('login',[AuthController::class,'login'])->name('login');

Route::post('logout',[AuthController::class,'logout'])->name('logout');

Route::middleware(['auth'])->group(function () {

    Route::get('author',[AuthorController::class,'index'])->name('authors');
    Route::get('author/fetch',[AuthorController::class,'fetchAuthors'])->name('authors.fetch');
    Route::get('author/view/{id}',[AuthorController::class,'show'])->name('authors.view');
    Route::get('author/add',[AuthorController::class,'add'])->name('authors.add');
    Route::post('author/store',[AuthorController::class,'store'])->name('authors.store');
    Route::get('author/books/{id}',[AuthorController::class,'showAuthorsBooks'])->name('authors.books');
    Route::get('author/{id}',[AuthorController::class,'destroy'])->name('authors.delete');

    Route::get('book',[BookController::class,'index'])->name('books');
    Route::post('book/store',[BookController::class,'store'])->name('books.store');
    Route::get('book/{id}',[BookController::class,'destroy'])->name('books.delete');


    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
});


